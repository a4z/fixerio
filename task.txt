C++ test task

The task is to create an app that converts between two currencies.

Create a command line application, so you can convert between 1 Us dollar to 8 Swedish krona, or 1 Swedish krona to Euros.

Minimum is: Input of amount, ability to choose currency and display converted amount similar to below. Design is entirely up to you.

 Requirements

Use the API from http://fixer.io/ to retrieve the currency rates.

We expect that you write unit tests

Please use GitHub or Bitbucket to publish your source code.

In the README of the repository we want a description of your approach and design description.
