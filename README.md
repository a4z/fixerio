# fixerio example application

This is a demo application based on a coding interview task.

The implementation is done with a focus to get the task done.  
It shall show 

 * some knowledge about today's C++ programming language 
 * usage of unit tests
 * usage of external C and C++ libraries
 * usage of build system

The program is not done to be 'perfect' and not meant to be used in real production.

NOTE: since this program was developed using the free version of fixer.io, which disables a lof of functionality, some work around for the conversion had to be implemented.  
Therefore, 100% accuracy for the conversion can not be guaranteed.

If you want a 100% accurate conversion, please deliver also a valid key for fixer.io, an implementation will be simple and can be done easily.

All source code in this project is released under the Mozilla Public License Version 2.0.

## build requirements 

* [libcurl](https://curl.haxx.se/libcurl/) for doing web/rest calls
* [rapidjson](http://rapidjson.org/) for json processing
* [argh](https://github.com/adishavit/argh) for command line parameter handling
* [doctest](https://github.com/onqtam/doctest) for unit testing

argh and doctest are included in the project (see include folder), 
libcurl and rapidjson need to be available on the system.

The build system used is CMake. It requires version 3.14, but you can set `cmake_minimum_required` to a lower version in the CMakeLists.txt

The implementation expects a compiler with C++17 support.

The application was developed on 

* Windows 10 using MSVC from Visual Studio 16.3.
* Linux, CentOS 7, gcc 8.3.1 
* Apple OSX, Apple LLVM version 10.0.1 (clang-1001.0.46.4)

### building the project

#### Windows

This project uses [vcpkg](https://github.com/microsoft/vcpkg), 
Microsoft's very own C++ package manager, to install the required dependencies on Windows.

Get [vcpkg](https://github.com/microsoft/vcpkg) and checkout the branch 2019.08

This default setup for this project and it's configure scripts expect 
vcpkg to be in C:\\vcpgk

Follow the installation instruction for [vcpkg](https://github.com/microsoft/vcpkg).

Once vcpkg is installed, use it to install the dependencies.

    .\vcpkg.exe install curl rapidjson doctest argh  

This will install the 32bit version of the dependencies, therefore the default build configuration of this project generat also 32 bit targets.
Other targets are possible but are not included for this sample project.

Once the dependencies are installed via [vcpkg](https://github.com/microsoft/vcpkg), run the configure.bat and the build.bat which are in the root folder of the project.

Binaries will be placed in the newly created folder build/Debug.

### building on Linux

The project just works on Linux, just make sure rapidjson-dev and libcurl-dev are installed, and you have a recent cmake version.

Either use [vcpkg](https://github.com/microsoft/vcpkg), similar to the Windows usage or provide the required package via your package manager.
rapidjson and libcurl-devel will be very likely available via a default package manager.   

### building on OSX

You can use [vcpkg](https://github.com/microsoft/vcpkg), or get rapidjson and curl via homebrew. 
In case of homebrew, is helps to also use a cmake installation provided via homebrew.

## internals

The top level directory contains the CMake file, some notes and some helper scripts for configuring and building the project.

The source code:

* src/fixerio_functions.hpp

    Contains the some of the testable parts of the application.
    For simplification, and to not have a internal static library, all functions are declared inline in this header file.

* src/fixerio.cpp

    The program. There are 2 functions that could resist in the functions header, 
    but since unit testing has already been demonstrated and to keep the time scope reasonalbe, this has been obmitted.

    The main function is simple top down, some commandline handling, validation, and than the action. 

* src/fixerio_tests.cpp

   Contains some unit tests in BDD style.


# usage

The following examples assume the fixer.io api key is accessible for the program, either via an environment variable FIXERIO_APIKEY or a fixerio.apiky file in the current working directory. The fixer.io api key can also be specified as a command line parameter  `--apikey=yourpersonalkey`. 

Run 

    fixerio --help

to get more information about the usage.


Once the program has been successfully build, it can be used as followed. 

    fixerio --from=EUR --to=SEK --amount=100
    100 EUR ~ 1072.48 SEK

To get a list of all available currencies, simply run 

    fixerio  --currencies
    AED, United Arab Emirates Dirham
    AFN, Afghan Afghani
    ALL, Albanian Lek
    AMD, Armenian Dram
    ANG, Netherlands Antillean Guilder
    AOA, Angolan Kwanza
    ...


## Windows special notes 

to have useful console output on Windows powershell
enable UTF8 output of applications. 

    $PSDefaultParameterValues['*:Encoding'] = 'utf8'

on other platforms, everything should just work fine ;-)

