#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest/doctest.h>

#include "fixerio_functions.hpp"

SCENARIO("geting the api key")
{

    GIVEN("an environment variable with a know value")
    {
        std::string known_value = "my_know_value_in_env";
        putenv((char*)"FIXERIO_APIKEY=my_know_value_in_env");

        WHEN("requesting the api key")
        {

            std::string env_value = fixerio_apikey();

            THEN("the value of the environemnt variable is returned")
            {
                CHECK_EQ(known_value, env_value);
            }
        }
    }

    GIVEN("no environment variable but a text file containing ")
    {

        putenv((char*)"FIXERIO_APIKEY=");

        const std::string known_value = "my_know_value_from_file";
        const std::string test_key_file = "test_fake_fixerio.apikey";

        std::ofstream keyfile_fake(test_key_file);
        REQUIRE(keyfile_fake.good());
        keyfile_fake << known_value;
        keyfile_fake.close();

        WHEN("requesting the api key")
        {

            std::string key_value = fixerio_apikey(test_key_file);

            THEN("the value of the environemnt variable is returned")
            {
                CHECK_EQ(key_value, known_value);
            }
        }
    }

    GIVEN("no environment variable and no keyfile")
    {

        putenv((char*)"FIXERIO_APIKEY=");

        WHEN("requesting the api key")
        {

            std::string key_value
                = fixerio_apikey("test_fixerio.apikey_that_does_not_exist");

            THEN("the value of the environemnt variable is returned")
            {
                CHECK(key_value.empty());
            }
        }
    }
}

SCENARIO("fetch url data")
{

    GIVEN("a valid URL")
    {
        std::string good_url = "http://data.fixer.io/api/latest";

        WHEN("makeing a request")
        {
            const auto [res, data] = fetch_url(good_url);
            THEN("a valid flaged result is returned")
            {
                CHECK_EQ(res, CURLE_OK);
            }
        }
    }

    GIVEN("a bod URL")
    {
        std::string bad_url = "http://data.fixer.iomanip/api/latest";
        WHEN("makeing a request")
        {
            const auto [res, data] = fetch_url(bad_url);
            THEN("an invalid flaged result is returned")
            {
                CHECK_NE(res, CURLE_OK);
            }
        }
    }
}