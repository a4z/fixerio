#pragma once

#include <cstdlib>
#include <curl/curl.h>

#include <exception>
#include <fstream>
#include <functional>
#include <memory>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <sstream>
#include <string>
#include <tuple>

/**
 * @brief return the fixerio api key
 *
 * If the environment variable FIXERIO_APIKEY exists, its content is returned.
 * Otherwise it tries to open the file fixerio.apikey in the current working
 * directory. If this file can not be found, an empty string is returned to flag
 * there is no value available.
 *
 *
 * @param keyfilename just for (test) dependency injection
 * @return std::string containing the API key or an empty string if no api key
 * could be find
 */
inline std::string fixerio_apikey(
    const std::string& keyfilename = "fixerio.apikey")
{

    std::string env_val;
    if (const char* via_env = std::getenv("FIXERIO_APIKEY")) {
        env_val = via_env;
    }

    if (!env_val.empty()) {
        return env_val;
    }

    std::ifstream keyfile(keyfilename.c_str());
    if (keyfile.good()) {
        std::string key;
        std::getline(keyfile, key);
        return key;
    }

    return {};
}

/**
 * @brief call fixerio
 *
 * The function fetches what ever url is passed in via libcurl.
 *
 * There is no error handling or other validation, the result is passed back to
 * the caller.
 *
 * @param url the url to use
 * @return std::tuple<CURLcode, std::string> a curl status code and a result of
 * the call
 */
inline std::tuple<CURLcode, std::string> fetch_url(const std::string& url)
{
    using cur_ptr = std::unique_ptr<CURL, decltype(&curl_easy_cleanup)>;
    cur_ptr curl { curl_easy_init(), &curl_easy_cleanup };

    if (!curl) {
        std::cerr << "can not initialize curl\n";
    }

    curl_easy_setopt(curl.get(), CURLOPT_URL, url.c_str());

    typedef size_t(callback_t)(char*, size_t, size_t, void*);
    callback_t* callback
        = [](char* ptr, size_t size, size_t nmemb, void* userp) {
              static_cast<std::string*>(userp)->append(ptr, size * nmemb);
              return size * nmemb;
          };

    std::string data;

    curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION, callback);
    curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, &data);

    CURLcode res = curl_easy_perform(curl.get());

    return std::make_tuple(res, data);
}

/**
 * @brief Check if the document returned by fixer.io is OK
 *
 * @param rj_doc a rapidjson document with the fixer.io response
 * @return true if the document has the success flag, false otherwise

 */
inline bool success_doc(const rapidjson::Document& rj_doc)
{
    if (rj_doc.HasMember("success")) {
        const auto& j_val = rj_doc["success"];
        if (j_val.IsBool()) {
            return j_val.GetBool();
        }
    }

    return false;
}

/**
 * @brief prints the (json) error message
 *
 * Simpley outputs the json error part of a fixer.io rest call
 * Since this is informative and human readable, no conversion or formating has
 * bee applied
 *
 *  NOTE, this function was used during development and is now unused
 *  however, since it might be re used later, it is still here
 *
 * @param rj_doc A rapidjsone document with the response of a  fixer.io rest
 * call
 * @return std::string A json string containing the error message
 */
inline std::string fixerio_error(const rapidjson::Document& rj_doc)
{
    if (rj_doc.HasMember("error")) {
        rapidjson::StringBuffer sb;
        rapidjson::Writer<rapidjson::StringBuffer> writer(sb);
        rj_doc["error"].Accept(writer);
        return sb.GetString();
    }
    return "no error information available, sorry";
}
