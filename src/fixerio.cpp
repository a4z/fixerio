#include <argh.h>
#include <iostream>

#include "fixerio_functions.hpp"

#include <argh.h>

const std::string help_text =
    R"~(
This application uses fixer.io to convert between currecnies:

    * Converting currencies:
    --from, --to, --amount, optional an --apikey

        --from, --to : 3 letters for the currency to convert
        --amount of money for convert --from into --to

    * To get a list of available currencies:
    --currencies, optional an --apikey

    * The fixer.io :API key
      fixer.io requires a valid API key,
      you can get on for free, visit fixer.io for more information
    --apikey 
        if omitted, the program tests, in this order, if
        * an environemnt variable FIXERIO_APIKEY is set
        * a file fixerio.apikey is in the current working directory
)~";

using json_handler_callback = std::function<bool(const rapidjson::Document&)>;
// it should be obvious that with some additional effort this could be unit
// tested but due to time limitations, for this coding test, this has been
// omitted since it should be obvious through the other unit tests that the
// author knows about, and understand, unit testing
bool handle_fixerio_call(
    const std::string& fixerurl, json_handler_callback json_handler)
{

    const auto [res, data] = fetch_url(fixerurl);
    if (res != CURLE_OK) {
        std::cerr << "curl call failed: " << curl_easy_strerror(res)
                  << std::endl;
        return false;
    }

    rapidjson::Document rj_doc;
    rj_doc.Parse(data.c_str());
    if (rj_doc.HasParseError()) {
        std::cerr << "did not receive a valid JSON document from fixerio\n";
        std::cerr << "--------------------------------------------------\n";
        std::cerr << data << "\n";
        return false;
    }

    if (!success_doc(rj_doc)) {
        std::cerr << "error reported by fixer.io, mabye your API key is "
                     "missing or incorrect? \n";
        std::cerr
            << "run --help to get information on how to set the API key \n";
        std::cerr << "--------------------------------------------------\n";
        std::cerr << fixerio_error(rj_doc) << std::endl;
        return false;
    }

    return json_handler(rj_doc);
}

bool print_currencies(const rapidjson::Document& rj_doc)
{

    const auto& symbols = rj_doc["symbols"];

    for (auto iter = symbols.MemberBegin(); iter != symbols.MemberEnd();
         ++iter) {
        std::cout << iter->name.GetString() << ", " << iter->value.GetString()
                  << "\n";
    }

    std::cout << std::endl;
    return true;
}

int main(int argc, char* argv[])
{

    // command line helpers are never a pleaseure,
    // so there are some ifs to handle
    argh::parser cmdl(argv);

    if (cmdl[{ "-h", "--help" }]) {
        std::cout << help_text << std::endl;
        return EXIT_SUCCESS;
    }

    // check if a parameter was given
    auto have_param = [&cmdl](const std::string& param) -> bool {
        return cmdl.params().find(param) != cmdl.params().end();
    };

    const std::string api_key
        = have_param("apikey") ? cmdl("apikey").str() : fixerio_apikey();

    if (api_key.empty()) {
        std::cerr << "No api key given, please re run with --help option for "
                     "more information";
        return EXIT_FAILURE;
    }

    // some helper function, not requested but useful to query currencies
    // if we want to know available currencies, we are done
    if (cmdl[{ "--currencies" }]) {
        const std::string symbols_url
            = "http://data.fixer.io/api/symbols?access_key=" + api_key;
        if (handle_fixerio_call(symbols_url, &print_currencies)) {
            return EXIT_SUCCESS;
        } else {
            return EXIT_FAILURE;
        }
    }

    // validation
    // if param was not given print it is missing
    auto ensure_param = [&cmdl, &have_param](const std::string& param) -> bool {
        if (!have_param(param)) {
            std::cerr << "missing parameter: " << param << std::endl;
            return false;
        }
        return true;
    };

    if (!(ensure_param("from") && ensure_param("to")
            && ensure_param("amount"))) {
        return EXIT_FAILURE;
    }

    // from here the actual conversion part

    const std::string convert_url
        = "http://data.fixer.io/api/latest?access_key=" + api_key + "&"
        + "symbols=" + cmdl("from").str() + "," + cmdl("to").str();

    float amount = 0.0f;
    cmdl("amount") >> amount;
    std::string from = cmdl("from").str();
    std::string to = cmdl("to").str();

    auto converter = [&](const rapidjson::Document& rj_doc) -> bool {
        const auto& rates = rj_doc["rates"];

        if (!rates.HasMember(from.c_str()) || !rates.HasMember(to.c_str())) {
            std::cerr << "Bad repsonse from fixer.io, please check you command "
                         "line paramters\n";
            return false;
        }

        const float rate_from = rates[from.c_str()].GetFloat();
        const float rate_to = rates[to.c_str()].GetFloat();
        const float result = amount / rate_from * rate_to;

        std::cout << amount << " " << from << " ~ " << result << " " << to
                  << std::endl;

        return true;
    };

    return handle_fixerio_call(convert_url, converter) ? EXIT_SUCCESS
                                                       : EXIT_FAILURE;
}